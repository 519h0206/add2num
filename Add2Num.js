class MyBigNumber {
  sum(stn1, stn2) {

    let result = ''; // Khởi tạo biến result với giá trị ban đầu là một chuỗi rỗng
    let remember = 0; // Khởi tạo biến remember với giá trị ban đầu là 0, biến này sẽ giữ giá trị nhớ khi cộng các chữ số của hai số
    let i = stn1.length - 1; // Khởi tạo biến i với giá trị ban đầu là độ dài của chuỗi stn1 trừ 1, vì ta sẽ duyệt chuỗi từ phải sang trái
    let j = stn2.length - 1; // Khởi tạo biến j với giá trị ban đầu là độ dài của chuỗi stn2 trừ 1, vì ta sẽ duyệt chuỗi từ phải sang trái

    if (typeof stn1 !== "string" || typeof stn2 !== "string") {
      throw new Error("Đầu vào không hợp lệ: cả hai tham số phải là chuỗi.");
    }

    if (!/^\d+$/.test(stn1) || !/^\d+$/.test(stn2)) {
      throw new Error(
        "Dữ liệu nhập không hợp lệ: cả hai tham số chỉ được chứa các chữ số."
      );
    }

    // Vòng lặp while này sẽ duyệt các chữ số của hai số từ phải sang trái cho đến khi hết một trong hai số
    while (i >= 0 || j >= 0) {
      // Lấy ra các chữ số từ phải sang trái của hai số, nếu đã hết số thì lấy chữ số 0
      const digit1 = i >= 0 ? parseInt(stn1.charAt(i)) : 0;
      const digit2 = j >= 0 ? parseInt(stn2.charAt(j)) : 0;

      // Cộng hai chữ số và giá trị nhớ lại với nhau
      const sum = digit1 + digit2 + remember;
      console.log(`Lấy ${digit1} + ${digit2} + nhớ ${remember} ta được ${sum}`)
      // Lấy chữ số hàng đơn vị của tổng sum và đưa vào chuỗi kết quả result
      result = (sum % 10) + result;
      // Lấy giá trị nhớ của phép cộng và lưu vào biến remember
      remember = Math.floor(sum / 10);
      console.log(`Lưu ${result} vào kêt quả và nhớ ${remember}`)

      // Giảm biến i và j để lần lượt lấy các chữ số tiếp theo
      i--;
      j--;

      // In giá trị của biến remember sau mỗi lần cộng các chữ số
    }

    // Nếu sau khi đã cộng hết các chữ số của hai số mà vẫn còn giá trị nhớ, thì ta thêm giá trị nhớ đó vào đầu chuỗi kết quả result
    if (remember !== 0) {
      result = remember + result;
    }

    // Trả về kết quả là chuỗi kết quả result
    return result;
  }
}

const myBigNumber = new MyBigNumber();
console.log("Kết quả của phép tính là: " + myBigNumber.sum("1234", "897"));
